#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <math.h>
#include <ctype.h>

#define BIGNUM 1.0e12
#define NUMROWS 16
#define NUMCOLS 2
#define NUMROWSCENT 3
#define NUMIT 3


int main(int argc, char *argv[])
{
	FILE *ipFile;
	FILE *centFile;
	FILE *opFile;
	int j;
	int k;
	float **x;
	float **cent;
	float **dist;
	int *bestCent;
	int it;

	if (argc!=4)
	{
		printf("format: kmeans <inputOpjects> <inputCentroids> <logFile>\n");
		exit(1);
	}
	
	argv++;
	if ((ipFile=fopen(*argv,"r"))==NULL)
	{
		printf("Error: can't open input file %s\n",*argv);
		exit(1);
	}

	argv++;
	if ((centFile=fopen(*argv,"r"))==NULL)
	{
		printf("Error: can't open input file %s\n",*argv);
		exit(1);
	}

	argv++;
	if ((opFile=fopen(*argv,"w"))==NULL)
	{
		printf("Error: can't open output file %s\n",*argv);
		exit(1);
	}

	printf("NUMROWS=%d NUMCOLS=%d\n",NUMROWS,NUMCOLS);

	x=(float **)calloc(NUMROWS,sizeof(float *));
	dist=(float **)calloc(NUMROWS,sizeof(float *));
	for (j=0; j<NUMROWS; j++)
	{
		x[j]=(float *)calloc(NUMCOLS,sizeof(float));
	}

	for (j=0; j<NUMROWS; j++)
	{
		for (k=0; k<NUMCOLS; k++) 
		{
			fscanf(ipFile,"%f ",&(x[j][k]));
		}
		fscanf(ipFile,"\n");
	}

	printf("number of data points = %d\n",NUMROWS);

	printf("NUMROWSCENT=%d, NUMCOLS=%d\n",NUMROWSCENT,NUMCOLS);

	cent=(float **)calloc(NUMROWSCENT,sizeof(float *));
	bestCent=(int *)calloc(NUMROWS,sizeof(int));
	for (j=0; j<NUMROWS; j++) 
	{
		dist[j]=(float *)calloc(NUMROWSCENT,sizeof(float));
	}
	
	for (j=0; j<NUMROWSCENT; j++)
	{
		cent[j]=(float *)calloc(NUMCOLS,sizeof(float));
	}

	for (j=0; j<NUMROWSCENT; j++)
	{
		for (k=0; k<NUMCOLS; k++) 
		{
			fscanf(centFile,"%f ",&(cent[j][k]));
		}
		fscanf(ipFile,"\n");
	}

	fprintf(opFile,"original\n");
	for (j=0; j<NUMROWSCENT; j++)
	{
		for (k=0; k<NUMCOLS; k++)
			fprintf(opFile,"%f ",cent[j][k]);
		fprintf(opFile,"\n");
	}

	for (it=0; it<NUMIT; it++)
	{
		float rMin;
		int count;

		count=0;

		for (j=0; j<NUMROWS; j++)
		{
			rMin=BIGNUM;
			for (k=0; k < NUMROWSCENT; k++)
			{
				dist[j][k]=0;

				dist[j][k]=sqrt(pow(x[j][0]-cent[k][0], 2)+pow(x[j][1]-cent[k][1], 2)); 
				/*printf(" point [%f;%f] cent [%f;%f] dist=%f\n", x[j][0], x[j][1], cent[k][0], cent[k][1], dist[j][k]);*/

				if (dist[j][k] < rMin)
				{
					bestCent[j]=k;
					rMin=dist[j][k];
					/*printf("bestCent[%d], rMin [%f]\n", bestCent[j] , rMin);*/
				}
			}
			count++;
			/*printf("\n\n\n\n");*/
		}

		for (k=0; k<NUMROWSCENT; k++)
		{
			float *cent_r;
			int tot;

			cent_r=(float *)calloc(NUMCOLS,sizeof(float));
			tot=0;
			for (j=0; j<NUMROWS; j++)
			{
				if (bestCent[j]==k)
				{
					int e;
					for (e=0; e<NUMCOLS; e++) 
						cent_r[e]+=x[j][e];
					tot++;
				}
			}
			int e;
			for (e=0; e<NUMCOLS; e++) 
				cent[k][e]=cent_r[e]/tot;
		}

		/* write centroids to file */
		fprintf(opFile,"\n\nit=%d\n",it);
		for (k=0; k<NUMROWSCENT; k++) 
		{
			int e;
			for (e=0; e<NUMCOLS; e++) 
				fprintf(opFile,"%f ",cent[k][e]);
			fprintf(opFile,"\n");
		}
		for (k=0; k<NUMROWSCENT; k++)
		{
			fprintf(opFile,"\nCluster %d\n",k);
			for (j=0; j<NUMROWS; j++)
			{
				if (bestCent[j]==k) 
					fprintf(opFile,"[%f;%f]\n",x[j][0], x[j][1]);
			}
		}
	}

	return(0);
}
